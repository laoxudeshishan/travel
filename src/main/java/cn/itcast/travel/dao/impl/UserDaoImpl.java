package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.UserDao;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class UserDaoImpl implements UserDao {
    DataSource dataSource;
    private JdbcTemplate template = new JdbcTemplate(JDBCUtils.getDataSource());

    /*
    * @MethodDescription: 
    根据用户名查询用户信息
    * @Return: cn.itcast.travel.domain.User
    * @author: xwx
    * @date: 2021/8/20 
    */
    @Override
    public User findByUsername(String username) {
        User user = null;
        //定义sql
        String sql = "select * from tab_user where username = ?";
        //执行sql
        try {
            user = template.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), username);
        } catch (Exception e) {
        }
        return user;

    }

    /*
    * @MethodDescription:
    将用户信息插入数据库
    * @Return: void
    * @author: xwx
    * @date: 2021/8/20
    */
    @Override
    public void save(User user) {
        //定义sql
        String sql = "insert into tab_user(username,password,name,birthday,sex,telephone,email,status,code)"
                + "values(?,?,?,?,?,?,?,?,?)";
        //执行sql
        template.update(sql, user.getUsername(),
                user.getPassword(),
                user.getName(),
                user.getBirthday(),
                user.getSex(),
                user.getTelephone(),
                user.getEmail(),
                user.getStatus(),
                user.getCode()
        );
    }

    /*
    * @MethodDescription: 
    根据激活码查询用户对象
    * @Return: cn.itcast.travel.domain.User
    * @author: xwx
    * @date: 2021/8/20 
    */
    @Override
    public User findByCode(String code) {
        User user = null;
        //定义sql
        String sql = "select * from tab_user where code=?";
        try {
            //若查询到user,将查询到的user返回
            user = template.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), code);
            return user;
        } catch (Exception e) {
            //若异常。返回null
            return null;
        }
    }
    /*
    * @MethodDescription:
    更新激活状态
    * @Return:
    * @author: xwx
    * @date: 2021/8/21
    */

    @Override
    public void updateStatus(User user) {
        //定义sql
        String sql = "update tab_user set status = 'Y' where Uid = ?";
        //执行sql
        template.update(sql, user.getUid());
    }

    /*
    * @MethodDescription:
    根据用户名密码查找用户
    * @Return: cn.itcast.travel.domain.User
    * @author: xwx
    * @date: 2021/8/21
    */
    @Override
    public User findByUsernameAndPassword(String username, String password) {
        User user = null;
        //定义sql
        String sql = "select * from tab_user where username = ? and password = ? ";
        //执行sql
        try {
            user = template.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), username, password);
            return user;
        } catch (Exception e) {
            return null;
        }
    }


}
