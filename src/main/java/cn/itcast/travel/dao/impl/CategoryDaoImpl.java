package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.CategoryDao;
import cn.itcast.travel.domain.Category;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class CategoryDaoImpl implements CategoryDao {
    //创建JdbcTemplate对象
    JdbcTemplate template = new JdbcTemplate(JDBCUtils.getDataSource());

    /*
    * @MethodDescription:
    查询分类列表
    * @Return: void
    * @author: xwx
    * @date: 2021/8/22
    */
    @Override
    public List<Category> findAll() {
        //1.定义sql
        String sql = "select * from tab_category";
        //2.执行sql
        return template.query(sql, new BeanPropertyRowMapper<Category>(Category.class));
    }
}
