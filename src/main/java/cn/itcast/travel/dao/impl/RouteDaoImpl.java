package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class RouteDaoImpl implements RouteDao {
    DataSource dataSource;
    JdbcTemplate template = new JdbcTemplate(JDBCUtils.getDataSource());

    /**
     * @return int
     * @Author xwx
     * @Description //TODO
     * 根据cid查询总记录数
     * @Date 11:15 上午 2021/8/24
     * @Param [cid]
     **/
    @Override
    public int findTotalCount(int cid, String rname) {
        //String sql = "select count(*) from tab_route where cid = ? ";
        //1.定义模板
        String sql = "select count(*) from tab_route where 1 = 1  ";
        //方便进行字符串拼接，定义一个StringBuilder
        StringBuilder sb = new StringBuilder(sql);
        //由于参数的不确定性，定义一个参数集合，用来储存参数
        List parameters = new ArrayList();
        //判断参数是否存在
        if (cid != 0) {
            sb.append(" and cid = ? ");
            parameters.add(cid);
        }
        if ((!"null".equals(rname)) && rname.length() > 0) {
            sb.append(" and rname like ? ");
            parameters.add("%" + rname + "%");
        }
        sql = sb.toString();
        int totalCount = template.queryForObject(sql, Integer.class, parameters.toArray());
        return totalCount;
    }

    /**
     * @return java.util.List<cn.itcast.travel.domain.Route>
     * @Author xwx
     * @Description //TODO
     * 查询某一页的数据
     * @Date 11:15 上午 2021/8/24
     * @Param [cid, start, pageSize]
     **/
    @Override
    public List<Route> findByPage(int cid, int start, int pageSize, String rname) {
        //String sql = "select * from tab_route where cid = ? limit ? , ? ";
        String sql = "select * from tab_route where 1 = 1 ";
        //由于参数的不确定性，定义一个参数集合，用来储存参数
        List parameters = new ArrayList();
        //方便进行字符串拼接，定义一个StringBuilder
        StringBuilder sb = new StringBuilder(sql);

        //判断参数是否存在
        if (cid != 0) {
            sb.append(" and cid = ? ");
            parameters.add(cid);
        }
        if ((!"null".equals(rname)) && rname.length() > 0) {
            sb.append(" and rname like ? ");
            parameters.add("%" + rname + "%");
        }
        sb.append("  limit ? , ? ");//分页条件
        parameters.add(start);
        parameters.add(pageSize);
        sql = sb.toString();
        List<Route> list = template.query(sql, new BeanPropertyRowMapper<Route>(Route.class), parameters.toArray());
        return list;
    }

    @Override
    public Route findOne(int rid) {
        //根据id去route表中查询route对象
        String sql = "select * from tab_route where rid = ? ";
        Route one = template.queryForObject(sql, new BeanPropertyRowMapper<Route>(Route.class), rid);
        return one;
    }
}
