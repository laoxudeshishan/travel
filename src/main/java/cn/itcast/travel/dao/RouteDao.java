package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Route;

import java.util.List;

public interface RouteDao {
    /**
     * @return int
     * @Author xwx
     * @Description //TODO
     * 根据cid查询总记录数
     * @Date 11:13 上午 2021/8/24
     * @Param [cid]
     **/
    public int findTotalCount(int cid, String rname);

    /**
     * @return java.util.List<cn.itcast.travel.domain.Route>
     * @Author xwx
     * @Description //TODO
     * 查询某一页的数据
     * @Date 11:14 上午 2021/8/24
     * @Param [cid, start, pageSize]
     **/
    public List<Route> findByPage(int cid, int start, int pageSize, String rname);


    public Route findOne(int rid);
}
