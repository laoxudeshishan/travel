package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Seller;

public interface SellerDao {
    /**
     * @return cn.itcast.travel.domain.Seller
     * @Author xwx
     * @Description //TODO
     * 跟据商家的id查询商家对象
     * @Date 4:00 下午 2021/8/25
     * @Param [id]
     **/
    public Seller findById(int id);
}
