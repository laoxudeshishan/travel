package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.UserDao;
import cn.itcast.travel.dao.impl.UserDaoImpl;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.UserService;
import cn.itcast.travel.util.MailUtils;
import cn.itcast.travel.util.UuidUtil;

import java.lang.annotation.ElementType;

public class UserServiceImpl implements UserService {
    UserDao dao = new UserDaoImpl();

    /*
    * @MethodDescription: 
    用户注册的方法
    * @Return: boolean
    * @author: xwx
    * @date: 2021/8/20 
    */
    @Override
    public boolean regist(User user) {
        //第一步，检验验证码是否正确
        //查询用户名，检测用户名是否被占用
        User result = dao.findByUsername(user.getUsername());
        //判断是否为null
        if (result != null) {
            //如果查询出来了，注册失败return false;
            return false;
        } else {
            //用户名未被注册,注册成功，储存用户信息
            //设置激活码与激活状态
            user.setCode(UuidUtil.getUuid());
            user.setStatus("N");
            dao.save(user);
            //发送激活邮件
            String content = "<a href='http://localhost:8080/travel/user/active?code=" + user.getCode() + "'>点击激活您在黑马旅游网的账号</a>";
            MailUtils.sendMail(user.getEmail(), content, "激活邮件");
            return true;
        }


    }

    /*
    * @MethodDescription: 
    激活用户
    * @Return: boolean
    * @author: xwx
    * @date: 2021/8/20 
    */
    @Override
    public boolean active(String code) {
        //1.根据激活码查询用户对象
        User user = dao.findByCode(code);
        if (user != null) {
            //2.调用dao修改激活状态
            dao.updateStatus(user);
            return true;
        } else {
            return false;
        }
    }

    /*
    * @MethodDescription: 
    登陆方法
    * @Return: cn.itcast.travel.domain.User
    * @author: xwx
    * @date: 2021/8/21 
    */
    @Override
    public User login(User user) {
        return dao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
    }
}
