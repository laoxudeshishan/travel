package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.dao.RouteImgDao;
import cn.itcast.travel.dao.SellerDao;
import cn.itcast.travel.dao.impl.RouteDaoImpl;
import cn.itcast.travel.dao.impl.RouteImgDaoImpl;
import cn.itcast.travel.dao.impl.SellerDaoImpl;
import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.domain.RouteImg;
import cn.itcast.travel.domain.Seller;
import cn.itcast.travel.service.RouteService;

import java.util.List;

//线路Service
public class RouteServiceImpl implements RouteService {
    //创建dao对象
    private RouteDao dao = new RouteDaoImpl();
    private RouteImgDao imgDao = new RouteImgDaoImpl();
    private SellerDao sellerDao = new SellerDaoImpl();

    /**
     * @return cn.itcast.travel.domain.PageBean<cn.itcast.travel.domain.Route>
     * @Author xwx
     * @Description //TODO
     * 根据类别进行线路的分页查询
     * @Date 11:02 上午 2021/8/24
     * @Param [cid, currentPage, pageSize]
     **/
    public PageBean<Route> pageQuery(int cid, int currentPage, int pageSize, String rname) {
        //封装pageBean
        PageBean<Route> pb = new PageBean<Route>();
        pb.setCurrentPage(currentPage);
        pb.setPageSize(pageSize);
        //查询totalCount并设置
        int totalCount = dao.findTotalCount(cid, rname);
        pb.setTotalCount(totalCount);
        //查询并设置当前页的数据集合
        int start = (currentPage - 1) * pageSize;//开始的记录数
        List<Route> list = dao.findByPage(cid, start, pageSize, rname);
        pb.setList(list);
        //计算总页数
        int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        pb.setTotalPage(totalPage);

        return pb;
    }

    @Override
    public Route findOne(String rid) {
        //1.根据rid查到route对象
        Route route = dao.findOne(Integer.parseInt(rid));
        //2.根据rid查询到图片集合信息
        List<RouteImg> imgs = imgDao.findByRid(Integer.parseInt(rid));
        //将集合设置到route对象中
        route.setRouteImgList(imgs);
        //根据route的sid，查询商家对象
        Seller seller = sellerDao.findById(route.getSid());
        route.setSeller(seller);
        return route;
    }

}
