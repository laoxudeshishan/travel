package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.CategoryDao;
import cn.itcast.travel.dao.impl.CategoryDaoImpl;
import cn.itcast.travel.domain.Category;
import cn.itcast.travel.service.CategoryService;
import cn.itcast.travel.util.JedisUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CategoryServiceImpl implements CategoryService {
    CategoryDao dao = new CategoryDaoImpl();

    @Override
    public List findAll() {
        //1.从redis中查询
        //获取jedis客户端
        Jedis jedis = JedisUtil.getJedis();
        //用sortedSet查询  //zrange 查询  zadd 储存
        Set<Tuple> categorys = jedis.zrangeWithScores("category", 0, -1);
        //2.判断查询的集合是否为空
        if (categorys == null || categorys.size() == 0) {
            //如果为空
            System.out.println("从数据库查询");
            //调用dao从数据库查询分类数据
            List<Category> all = dao.findAll();
            //将数据储存至redis
            for (int i = 0; i < all.size(); i++) {
                jedis.zadd("category", all.get(i).getCid(), all.get(i).getCname());
            }
            return all;
        } else {
            //如果不为空
            System.out.println("使用缓存查询");
            //将set数据存入list
            List<Category> list = new ArrayList<>();
            for (Tuple tuple : categorys) {
                Category category = new Category();
                category.setCname(tuple.getElement());
                category.setCid((int) tuple.getScore());
                list.add(category);
            }
            return list;
        }
    }
}
