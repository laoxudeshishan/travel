package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.service.RouteService;
import cn.itcast.travel.service.impl.RouteServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet("/route/*")
public class RouteServlet extends BaseServlet {
    RouteService service = new RouteServiceImpl();

    /*
    * @MethodDescription:
    分页查询
    * @Return: void
    * @author: xwx
    * @date: 2021/8/23 
    */
    public void pageQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //接收参数
        String currentPageStr = request.getParameter("currentPage");
        String pageSizeStr = request.getParameter("pageSize");
        String cidStr = request.getParameter("cid");
        //接收线路名称
        String rname = request.getParameter("rname");
        int cid = 0;
        int currentPage = 0;
        int pageSize = 5;
        //处理参数
        if (cidStr != null && cidStr.length() > 0) {
            cid = Integer.parseInt(cidStr);
        }

        if (currentPageStr != null && currentPageStr.length() > 0) {
            currentPage = Integer.parseInt(currentPageStr);
        }//第一次访问的时候，currentPage没有数据,默认为1
        else {
            currentPage = 1;
        }

        if (pageSizeStr != null && pageSizeStr.length() > 0) {
            pageSize = Integer.parseInt(pageSizeStr);
        }//如果不给服务器传每页显示几条数据,默认显示五条记录
        else {
            pageSize = 5;
        }

        //调用service查询pageBean对象
        PageBean<Route> pb = service.pageQuery(cid, currentPage, pageSize, rname);
        //将pb序列化为json返回
        writeValue(pb, response);

    }

    /**
     * @return void
     * @Author xwx
     * @Description //TODO
     * 根据id查询一个旅游线路的详细信息
     * @Date 2:59 下午 2021/8/25
     * @Param [request, response]
     **/
    public void findOne(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.接收id
        String rid = request.getParameter("rid");
        //2.调用service查询route对象
        Route route = service.findOne(rid);
        //3.转为json写回客户端
        writeValue(route, response);

    }
}
