package cn.itcast.travel.web.servlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //完成方法的分发
        //1.获取请求路径
        String uri = request.getRequestURI(); //返回/user/xxx
        //2.获取方法名称
        String methodName = uri.substring(uri.lastIndexOf("/") + 1);
        //3.获取方法对象
        try {
            Method method = this.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);

            //4.执行方法
            method.invoke(this, request, response);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /*
    * @MethodDescription:
    直接将传入obj对象序列化为json，并写会客户端
    * @Return: void
    * @author: xwx
    * @date: 2021/8/22
    */
    public void writeValue(Object obj, HttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            response.setContentType("application/json;charset=utf-8");
            mapper.writeValue(response.getOutputStream(), obj);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
    * @MethodDescription:
    将传入对象序列化为json并以字符串形式返回
    * @Return: java.lang.String
    * @author: xwx
    * @date: 2021/8/22
    */
    public String writeValueAsString(Object obj) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        String s = mapper.writeValueAsString(obj);
        return s;
    }
}
