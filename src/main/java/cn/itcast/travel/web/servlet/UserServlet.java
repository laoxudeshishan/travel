package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.ResultInfo;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.UserService;
import cn.itcast.travel.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.apache.commons.beanutils.BeanUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@WebServlet("/user/*")
public class UserServlet extends BaseServlet {
    //声明
    private UserService service = new UserServiceImpl();

    /*
    * @MethodDescription: 
    注册的servlet
    * @Return: void
    * @author: xwx
    * @date: 2021/8/21 
    */
    public void regist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //第一步：验证码校验
        //1.获取用户输入的验证码
        String check = request.getParameter("check");
        //2.从session中获取验证码
        HttpSession session = request.getSession();
        String checkcode_server = (String) session.getAttribute("CHECKCODE_SERVER");
        //验证码一旦获取，为了防止复用，应该从session中删除掉
        session.removeAttribute("CHECKCODE_SERVER");
        //比较
        if (checkcode_server == null || !checkcode_server.equalsIgnoreCase(check)) {
            //验证码错误
            ResultInfo resultInfo = new ResultInfo();
            //设置响应信息
            resultInfo.setErrorMsg("验证码错误");
            resultInfo.setFlag(false);
            //将info对象序列化为json，响应给客户端
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(resultInfo);
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().write(json);
            return;
        }

        //1.获取数据
        Map<String, String[]> map = request.getParameterMap();
        //2.封装对象
        User user = new User();
        try {
            BeanUtils.populate(user, map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //3.调用service完成注册
        boolean flag = service.regist(user);
        ResultInfo info = new ResultInfo();
        if (flag) {
            //注册成功，跳转到注册成功的页面
            info.setFlag(true);

        } else {
            info.setFlag(false);
            info.setErrorMsg("注册失败！");
        }
        //将info对象序列化为jason，写回客户端
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(info);
        //设置响应的数据类型
        response.setContentType("application/json;charset=utf-8");
        //将json数据写回客户端
        response.getOutputStream().write(json.getBytes(StandardCharsets.UTF_8));

    }

    /*
    * @MethodDescription: 
    登陆的servlet
    * @Return: void
    * @author: xwx
    * @date: 2021/8/21 
    */
    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {//1.获取用户信息
        Map<String, String[]> map = request.getParameterMap();
        //第一步：验证码校验
        //1.获取用户输入的验证码
        String check = request.getParameter("check");
        //2.从session中获取验证码
        HttpSession session = request.getSession();
        String checkcode_server = (String) session.getAttribute("CHECKCODE_SERVER");
        //验证码一旦获取，为了防止复用，应该从session中删除掉
        session.removeAttribute("CHECKCODE_SERVER");
        if(checkcode_server==null) checkcode_server = CheckCodeServlet.getCheckCode();
        if (checkcode_server.equalsIgnoreCase(check)) {
            //2.封装对象
            User user = new User();
            try {
                BeanUtils.populate(user, map);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            //3.调用service登陆

            User u = service.login(user);
            //4.判断是否查询到用户存在
            ResultInfo resultInfo = new ResultInfo();
            if (u == null) {
                //用户不存在，用户名或密码错误
                //设置响应消息
                resultInfo.setFlag(false);
                resultInfo.setErrorMsg("用户名或密码错误");
            } else {
                //用户存在，检验是否激活
                String status = u.getStatus();
                if ("Y".equals(status)) {
                    //已经被激活
                    //跳转至登陆成功页面
                    resultInfo.setFlag(true);
                    //将用户信息储存到session
                    request.getSession().setAttribute("user", u);
                } else {
                    //尚未激活
                    //响应消息
                    resultInfo.setFlag(false);
                    resultInfo.setErrorMsg("用户尚未激活，请先激活");
                }
            }
            //将响应消息转为json
            ObjectMapper mapper = new ObjectMapper();
            String s = mapper.writeValueAsString(resultInfo);
            //将json数据写入客户端
            response.setContentType("application/json;utf-8");
            response.getWriter().write(s);
        } else {
            //将错误信息储存入info对象
            ResultInfo resultInfo = new ResultInfo();
            resultInfo.setErrorMsg("验证码错误");
            resultInfo.setFlag(false);
            //将响应的数据转为json
            ObjectMapper mapper = new ObjectMapper();
            String s = mapper.writeValueAsString(resultInfo);
            //将json写入客户端
            response.setContentType("application/json;utf-8");
            response.getWriter().write(s);
        }

    }

    /*
    * @MethodDescription:
    完成页眉部分用户名字显示的servlet
    * @Return: void
    * @author: xwx
    * @date: 2021/8/21
    */
    public void findOne(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //从session里获取登陆用户
        Object user = request.getSession().getAttribute("user");
        //将user写回客户端
        ObjectMapper mapper = new ObjectMapper();
        //设置响应格式
        response.setContentType("application/json;charset=utf-8");
        mapper.writeValue(response.getOutputStream(), user);
    }

    /*
    * @MethodDescription:
    完成用户退出的servlet
    * @Return: void
    * @author: xwx
    * @date: 2021/8/21
    */
    public void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.销毁session
        request.getSession().invalidate();
        //2.重定向至登陆页面
        response.sendRedirect(request.getContextPath() + "/login.html");
    }


    /*
    * @MethodDescription:
    用户激活
    * @Return: void
    * @author: xwx
    * @date: 2021/8/21
    */
    public void active(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取激活码
        String code = request.getParameter("code");
        //2.判断激活码是否为空
        if ("".equals(code) || code == null) {
            //如果激活码为空
            response.getWriter().write("小伙子，别乱搞");
        } else {
            //3.调用service查询
            UserService service = new UserServiceImpl();
            boolean flag = service.active(code);
            String msg = null;
            if (flag) {
                //激活成功
                msg = "激活成功!请<a href='/travel/login.html'>登陆</a>";
            } else {
                //激活失败
                msg = "激活失败，请联系管理员！";
            }
            // 设置响应编码
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write(msg);
        }
    }
}
